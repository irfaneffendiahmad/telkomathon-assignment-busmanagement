let arrPenumpang = [];


// fungsi-fungsi untuk mengisi penumpang minibus
function isiPenumpang() {
    tambahPenumpang();
}

function tambahPenumpang() {
    let tambahPenumpang = prompt('Masukkan nama penumpang!');

    if (tambahPenumpang == '') {
        alert('Nama penumpang tidak boleh kosong!');
    } else if (!tambahPenumpang) {
        return;
    } else {
        // Cek nama penumpang
        if(arrPenumpang.indexOf(tambahPenumpang) !== -1) {
            alert('Nama sudah terdaftar sebagai penumpang!');
        } else {
            arrPenumpang.push(tambahPenumpang);

            console.log(arrPenumpang);
            alert(`Jumlah penumpang saat ini ada ${arrPenumpang.length} orang dari kapasitas 7 orang dengan nama penumpang: \n${arrPenumpang}`);
            if(typeof(arrPenumpang[0]) === "undefined"){
                document.getElementById("penumpang1").innerHTML = 'Penumpang 1';
            } else { 
                document.getElementById("penumpang1").innerHTML = arrPenumpang[0];
            }
            if(typeof(arrPenumpang[1]) === "undefined"){
                document.getElementById("penumpang2").innerHTML = 'Penumpang 2 ';
            } else { 
                document.getElementById("penumpang2").innerHTML = arrPenumpang[1];
            }
            if(typeof(arrPenumpang[2]) === "undefined"){
                document.getElementById("penumpang3").innerHTML = 'Penumpang 3';
            } else { 
                document.getElementById("penumpang3").innerHTML = arrPenumpang[2];
            }
            if(typeof(arrPenumpang[3]) === "undefined"){
                document.getElementById("penumpang4").innerHTML = 'Penumpang 4';
            } else { 
                document.getElementById("penumpang4").innerHTML = arrPenumpang[3];
            }
            if(typeof(arrPenumpang[4]) === "undefined"){
                document.getElementById("penumpang5").innerHTML = 'Penumpang 5';
            } else { 
                document.getElementById("penumpang5").innerHTML = arrPenumpang[4];
            }
            if(typeof(arrPenumpang[5]) === "undefined"){
                document.getElementById("penumpang6").innerHTML = 'Penumpang 6';
            } else { 
                document.getElementById("penumpang6").innerHTML = arrPenumpang[5];
            }
            if(typeof(arrPenumpang[6]) === "undefined"){
                document.getElementById("penumpang7").innerHTML = 'Penumpang 7';
            } else { 
                document.getElementById("penumpang7").innerHTML = arrPenumpang[6];
            }
    
            tambahPenumpang2();
        } 
    }
}

function tambahPenumpang2() {
    if (arrPenumpang.length == 7) {
        alert('Kapasitas Mini Bus sudah penuh!');
    } else {
        let confTambahPenumpang = confirm('Ingin menambah penumpang?');
        if (confTambahPenumpang == true) {
            tambahPenumpang();
        }
    } 
}


// fungsi-fungsi untuk mengupdate penumpang minibus
function updatePenumpang() {
    if (arrPenumpang.length == 0) {
        alert('Saat ini belum ada penumpang terdaftar!');
    } else {
        let gantiPenumpang = arrPenumpang.indexOf(prompt(`Ini adalah nama penumpang yang telah terdaftar \n${arrPenumpang} \n\n Masukkan nama penumpang yang akan diganti!`));

        arrPenumpang[gantiPenumpang] = prompt('Masukkan nama penumpang pengganti');
        
        console.log(arrPenumpang);
        if(typeof(arrPenumpang[0]) === "undefined"){
            document.getElementById("penumpang1").innerHTML = 'Penumpang 1';
        } else { 
            document.getElementById("penumpang1").innerHTML = arrPenumpang[0];
        }
        if(typeof(arrPenumpang[1]) === "undefined"){
            document.getElementById("penumpang2").innerHTML = 'Penumpang 2 ';
        } else { 
            document.getElementById("penumpang2").innerHTML = arrPenumpang[1];
        }
        if(typeof(arrPenumpang[2]) === "undefined"){
            document.getElementById("penumpang3").innerHTML = 'Penumpang 3';
        } else { 
            document.getElementById("penumpang3").innerHTML = arrPenumpang[2];
        }
        if(typeof(arrPenumpang[3]) === "undefined"){
            document.getElementById("penumpang4").innerHTML = 'Penumpang 4';
        } else { 
            document.getElementById("penumpang4").innerHTML = arrPenumpang[3];
        }
        if(typeof(arrPenumpang[4]) === "undefined"){
            document.getElementById("penumpang5").innerHTML = 'Penumpang 5';
        } else { 
            document.getElementById("penumpang5").innerHTML = arrPenumpang[4];
        }
        if(typeof(arrPenumpang[5]) === "undefined"){
            document.getElementById("penumpang6").innerHTML = 'Penumpang 6';
        } else { 
            document.getElementById("penumpang6").innerHTML = arrPenumpang[5];
        }
        if(typeof(arrPenumpang[6]) === "undefined"){
            document.getElementById("penumpang7").innerHTML = 'Penumpang 7';
        } else { 
            document.getElementById("penumpang7").innerHTML = arrPenumpang[6];
        }
    }
}


// fungsi-fungsi untuk menghapus penumpang minibus terdaftar
function hapusPenumpang() {
    if (arrPenumpang.length == 0) {
        alert('Saat ini belum ada penumpang terdaftar!');
    } else {
        let hapusPenumpang = arrPenumpang.indexOf(prompt(`Ini adalah nama penumpang yang telah terdaftar \n${arrPenumpang} \n\n Masukkan nama penumpang yang akan dihapus!`));

        arrPenumpang.splice(hapusPenumpang,1);

        console.log(arrPenumpang);
        if(typeof(arrPenumpang[0]) === "undefined"){
            document.getElementById("penumpang1").innerHTML = 'Penumpang 1';
        } else { 
            document.getElementById("penumpang1").innerHTML = arrPenumpang[0];
        }
        if(typeof(arrPenumpang[1]) === "undefined"){
            document.getElementById("penumpang2").innerHTML = 'Penumpang 2 ';
        } else { 
            document.getElementById("penumpang2").innerHTML = arrPenumpang[1];
        }
        if(typeof(arrPenumpang[2]) === "undefined"){
            document.getElementById("penumpang3").innerHTML = 'Penumpang 3';
        } else { 
            document.getElementById("penumpang3").innerHTML = arrPenumpang[2];
        }
        if(typeof(arrPenumpang[3]) === "undefined"){
            document.getElementById("penumpang4").innerHTML = 'Penumpang 4';
        } else { 
            document.getElementById("penumpang4").innerHTML = arrPenumpang[3];
        }
        if(typeof(arrPenumpang[4]) === "undefined"){
            document.getElementById("penumpang5").innerHTML = 'Penumpang 5';
        } else { 
            document.getElementById("penumpang5").innerHTML = arrPenumpang[4];
        }
        if(typeof(arrPenumpang[5]) === "undefined"){
            document.getElementById("penumpang6").innerHTML = 'Penumpang 6';
        } else { 
            document.getElementById("penumpang6").innerHTML = arrPenumpang[5];
        }
        if(typeof(arrPenumpang[6]) === "undefined"){
            document.getElementById("penumpang7").innerHTML = 'Penumpang 7';
        } else { 
            document.getElementById("penumpang7").innerHTML = arrPenumpang[6];
        }
    }
}